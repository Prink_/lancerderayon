#include "camera.hpp"
#include "pixelmap.hpp"
#include "rayon.hpp"
#include "Scene.hpp"
#include <stdio.h>

void Camera::Haut(const glm::vec3 & h) {

	haut = h - (dir * (dir * h));
	
	
	if (glm::length(haut) != 0.0)
		haut = glm::normalize(haut);
	
}

void Camera::Calculer_image(Pixelmap & pm, Scene &sc, int complexite) const {
	glm::vec3 foyer; // Foyer optique de la camera
	glm::vec3 droite; // Vecteur partant sur la droite dans le plan de l'ecran
	float dx, dy; // dimension des macro-pixels
	int x, y; // Position dans l'image du pixel en cours de calcul
	glm::vec3 hg; // Position du pixel au centre du premier macro-pixel de l'ecran (en haut a gauche)
	glm::vec3 pt; // Position de l'intersection entre le rayon a lancer et l'ecran
	Rayon ray; // Rayon a lancer
	glm::vec3 vect; // Vecteur directeur du rayon a lancer
	int index; // Indice du pixel traite
	float percent = 0.0f;

	// On calcule la position du foyer de la camera
	foyer = centre - (dir * dist);
	
	//std::cout << "centre : " << haut.x << " " << haut.y <<" " << haut.z << std::endl; 
	// On calcule le vecteur unitaire "droite" du plan
	droite = glm::cross(haut, dir);
	

	// On calcule le deltaX et le deltaY
	dx = largeur / pm.Largeur();
	dy = hauteur / pm.Hauteur();

	// On calcule la position du premier point de l'ecran que l'on calculera
	hg = centre + (droite * ((dx / 2) - (largeur / 2))) + (haut * ((hauteur / 2) - (dy / 2)));

	// Pour chaque pixel de l'image a calculer
	index = 0;
	std::cout << "Rendering..." << std::endl;
	printf("[ Please wait... ] - 0%% - (%d threads)", omp_get_num_threads());
	#pragma omp parallel for schedule(dynamic,10) shared(pm) private(x,y)
	for (y = 0; y <  pm.Hauteur(); y++) {
		for (x = 0; x < pm.Largeur(); x++) {
			// On calcule la position dans l'espace de ce point
			/*pt = hg + (droite * (dx * x)) - (haut * (dy * y));

			// On prepare le rayon qui part du foyer et qui passe par ce point
			vect = pt - foyer;
			vect = glm::normalize(vect);

			// Et on enregistre la couleur du rayon dans l'image
			/*ray.Orig(pt);
			ray.Vect(vect);
			pm.Map(x,y,ray.Lancer(sc, complexite));*/

			pm.Map(x, y, render(x, y, dx, dy, hg, haut, droite, foyer, sc, complexite));
		}
		if (omp_get_thread_num() == 0) {
			percent = (1.0f * y * pm.Largeur() + (1.0f * x)) * 100.0f / (1.0f *pm.Largeur() * pm.Hauteur());
			printf("\r");
			printf("[");
			for (int i = 0; i < 20; i++) {
				if (percent / 5.0f < i) {
					printf(" ");
				}
				else {
					printf("|");
				}
			}
			printf("] - %d%% - (%d threads)", (int)percent, omp_get_num_threads());
		}
	}
	std::cout << std::endl << "Done!" << std::endl;
}


