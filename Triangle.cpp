#include "stdafx.h"
#include "Triangle.h"

Triangle::Triangle()
{
	this->vertices.push_back(glm::vec3(0, 0, 0));
	this->vertices.push_back(glm::vec3(0, 1, 0));
	this->vertices.push_back(glm::vec3(0, 0, 1));

	this->position = this->vertices[0];
}

Triangle::Triangle(glm::vec3 A, glm::vec3 B, glm::vec3 C) {
	this->vertices.push_back(A);
	this->vertices.push_back(B);
	this->vertices.push_back(C);

	this->position = this->vertices[0];
}


Triangle::~Triangle()
{
}

bool Triangle::calculIntersection(const Rayon &ray, std::vector<Intersection>& I)
{
	//M�ller�Trumbore intersection algorithm
	glm::vec3 e1 = vertices[1] - vertices[0];
	glm::vec3 e2 = vertices[2] - vertices[0];

	glm::vec3 P = glm::cross(ray.Vect(), e2);
	float d = glm::dot(e1, P);
	if (d != 0) {
		glm::vec3 T = ray.Orig() - vertices[0];

		float u = glm::dot(T, P) / d;
		if (u > 0.0f && u < 1.0f) {
			glm::vec3 Q = glm::cross(T, e1);

			float v = glm::dot(ray.Vect(), Q) / d;
			if (v > 0.0f && u + v < 1.0f) {
				float t = glm::dot(e2, Q) / d;
				if (t > 0) {
					//std::cout << "ahahaha" << std::endl;
					Intersection i = Intersection();
					i.Objet = this;
					i.normal = glm::normalize(glm::cross(e1, e2));
					i.dist = t;
					i.position = ray.Orig() + ray.Vect() * t;
					I.push_back(i);
					return true;
				}
			}
		}
	}
	return false;
}

glm::vec3 Triangle::getMinPos()
{
	return glm::vec3(std::min({ vertices[0].x, vertices[1].x, vertices[2].x }),
		std::min({ vertices[0].y, vertices[1].y, vertices[2].y }),
		std::min({ vertices[0].z, vertices[1].z, vertices[2].z }));
}

glm::vec3 Triangle::getMaxPos()
{
	return glm::vec3(std::max({ vertices[0].x, vertices[1].x, vertices[2].x }),
		std::max({ vertices[0].y, vertices[1].y, vertices[2].y }),
		std::max({ vertices[0].z, vertices[1].z, vertices[2].z }));
}
