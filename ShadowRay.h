#pragma once
#include "rayon.hpp"
class ShadowRay : public Rayon
{
public:
	ShadowRay();
	~ShadowRay();
	float lightDist;

	glm::vec3 Lancer(Scene &sc, int recur) const;
};

