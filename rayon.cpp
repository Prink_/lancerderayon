#include "rayon.hpp"
#include "RefractionRay.h"
#include "Objet3D.hpp"

// --------------------------------------------------------------------------
//
// Methodes (et operations) sur les rayons
//
// --------------------------------------------------------------------------

//Fresnel's Equations
float fresnel(glm::vec3 v, glm::vec3 N, float n1, float n2) {
	float cosOi = -glm::dot(v, N);
	float sin2Ot = powf(n1 / n2, 2.0f) * (1.0f - powf(cosOi, 2.0f));
	float cosOt = sqrt(1.0f - sin2Ot);
	if (sin2Ot <= 1.0f) {
		return (powf((n1 * cosOi - n2 * cosOt) / (n1 * cosOi + n2 * cosOt), 2.0f) + powf((n2 * cosOi - n1 * cosOt) / (n2 * cosOi + n1 * cosOt), 2.0f)) / 2.0f;
	}
	else {
		return 1.0f;
	}
}

//Schlick's approximation
float schlick(glm::vec3 v, glm::vec3 N, float n1, float n2) {
	float cosOi = -glm::dot(v, N);
	float sin2Ot = powf(n1 / n2, 2.0f) * (1.0f - powf(cosOi, 2.0f));
	float cosOt = sqrt(1.0f - sin2Ot);
	if (sin2Ot <= 1.0f) {
		float R0 = powf((n1 - n2) / (n1 + n2), 2.0f);
		if (n1 <= n2) {
			return R0 + (1.0f - R0)*powf((1 - cosOi), 5.0f);
		}
		else {
			return R0 + (1.0f - R0)*powf((1 - cosOt), 5.0f);
		}
	}
	else {
		return 1.0f;
	}
}

std::vector<Intersection> Rayon::intersectScene(Scene &sc) const {
	std::vector<Intersection> interectedObjects;
	
	// Calcul des intersections du rayon avec la sc�ne
	for (auto obj : sc.listObjets) {
		obj->calculIntersection(*this, interectedObjects);
	}

	return interectedObjects;
}

Intersection Rayon::closestIntersection(std::vector<Intersection> interectedObjects) const {
	float dMin = 100000;
	Intersection I;

	for (auto i : interectedObjects) {
		if (i.dist < dMin) {
			dMin = i.dist;
			I = i;
		}
	}

	return I;
}

glm::vec3 Rayon::calculateIntersectionColor(Intersection I, Scene& sc, int recur) const {
	glm::vec3 res = sc.bgColor;
	if (I.Objet != NULL) {
		if (I.Objet->isLight)
			return I.Objet->color;

		glm::vec3 IA = glm::vec3(0.0f), IFr = glm::vec3(0.0f), IFl = glm::vec3(0.0f);
		float R = 0.0f;
		float T = 0.0f;

		//Ambiant
		IA = I.Objet->mat->calculateColor(*this, sc, I);

		if ((I.Objet->Kfr > 0.0f && I.Objet->Kfl > 0.0f)) { //Si l'objet � un coef de r�flection ou de r�fraction
			//R = fresnel(this->Vect, I.normal, this->rIndex, I.Objet->rIndex);
			R = schlick(this->vect, I.normal, this->rIndex, I.Objet->rIndex);
			T = 1.0f - R;
		}
		else {
			R = I.Objet->Kfl;
			T = I.Objet->Kfr;
		}

		//Reflection
		if (R > 0.0f && recur > 0) {
			Rayon RFl = Rayon();
			RFl.rIndex = this->rIndex;
			RFl.Vect(glm::reflect(this->Vect(), I.normal));
			RFl.Orig(I.position + EPSILON * RFl.Vect());
			IFl = RFl.Lancer(sc, recur - 1);
		}

		//Refraction
		if (T > 0.0f && recur > 0) {
			glm::vec3 refract = glm::refract(this->Vect(), I.normal, this->rIndex / I.Objet->rIndex);
			RefractionRay RFr = RefractionRay();
			RFr.rIndex = I.Objet->rIndex;
			RFr.I1 = I;
			RFr.Vect(refract);
			RFr.Orig(I.position + EPSILON * RFr.Vect());
			IFr = RFr.Lancer(sc, recur - 1);
		}

		res = I.Objet->Ka * IA + R * IFl + T * IFr;
	}
	return res;
}

glm::vec3 Rayon::IPosition(float t) const
{
	return this->orig + this->vect * t;
}

glm::vec3 Rayon::Lancer(Scene &sc, int recur) const {
	glm::vec3 res; // Couleur resultante	
	Intersection I;
	
	std::vector<Intersection> interectedObjects = std::vector<Intersection>(intersectScene(sc));

	// Traiter l'intersection la plus proche de l'origine
	I.Objet = NULL;
	I = closestIntersection(interectedObjects);

	// Calculer et retourner la couleur du rayon
	res = sc.bgColor;
	if (I.Objet != NULL) {
		res = calculateIntersectionColor(I, sc, recur);
	}

	return res;
}