#ifndef SCENE_HPP
#define SCENE_HPP

#include "Objet3D.hpp"
#include "Light.h"
#include <vector>

class Light;

class Scene
{
public:
	Scene();
	~Scene();
	void ajouterObjet(Objet3D *o);
	void addLight(Light* l);


	// Contient les elements de la scene
	std::vector < Objet3D*> listObjets;
	std::vector < Light*> lightList;

	glm::vec3 ambiantLight;
	glm::vec3 bgColor;
	
};


#endif