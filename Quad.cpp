#include "stdafx.h"
#include "Quad.h"


Quad::Quad() {
	this->position = glm::vec3(0.0);
	this->W = glm::vec3(1.0,0.0,0.0);
	this->H = glm::vec3(0.0,0.0,1.0);
	this->normal = glm::normalize(glm::cross(this->W, this->H));
	this->color = glm::vec3(0.2, 0.4, 0.8);
}

Quad::Quad(glm::vec3 A, glm::vec3 B, glm::vec3 C) {
	this->position = A;
	this->W = B - A;
	this->H = C - A;
	this->normal = glm::normalize(glm::cross(this->W, this->H));
	this->color = glm::vec3(0.2, 0.4, 0.8);
}

Quad::Quad(glm::vec3 O, glm::vec3 N, glm::vec3 H, glm::vec3 W) {
	this->position = O;
	this->W = W;
	this->H = H;
	this->normal = N;
	this->color = glm::vec3(0.2, 0.4, 0.8);
}

Quad::~Quad()
{
}

float triArea(glm::vec3 A, glm::vec3 B, glm::vec3 C) {
	float a, b, c, s;
	a = glm::distance(A, B);
	b = glm::distance(B, C);
	c = glm::distance(C, A);
	s = 1.0f / 2.0f * (glm::distance(A,B)+ glm::distance(B, C)+ glm::distance(C, A));
	return sqrt(s*(s-a)*(s-b)*(s-c));
}

bool Quad::calculIntersection(const Rayon &ray, std::vector<Intersection>& I)
{
	//Parallelism test. Coincidence is not rendered thus return false as well.
	float theta = glm::dot(this->normal, ray.Vect());
	if (theta != 0) {
		Intersection i = Intersection();
		i.normal = this->normal;
		i.Objet = this;
		i.dist = glm::dot(this->normal, this->position - ray.Orig()) / theta;
		i.position = ray.Orig() + i.dist * ray.Vect();

		if (i.dist > 0 
			&& glm::dot(this->position, W) <= glm::dot(i.position, W) 
			&& glm::dot(i.position, W) <= glm::dot(this->position + W, W) 
			&& glm::dot(this->position, H) <= glm::dot(i.position, H) 
			&& glm::dot(i.position, H) <= glm::dot(this->position + H, H)
		) {
			//std::cout << theta << std::endl;
			I.push_back(i);
			return true;
		}
	}

	return false;
}

