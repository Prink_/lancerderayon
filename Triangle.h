#pragma once
#include "Objet3D.hpp"

class Triangle : public Objet3D
{
public:
	Triangle();
	Triangle(glm::vec3 A, glm::vec3 B, glm::vec3 C);
	~Triangle();



	std::vector < glm::vec3 > vertices;

	bool calculIntersection(const Rayon &, std::vector<Intersection> & I);
	glm::vec3 getMinPos();
	glm::vec3 getMaxPos();
};

