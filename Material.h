#pragma once
#include <glm/glm.hpp>
#include <algorithm>

#include "rayon.hpp"
class Rayon;
class Objet3D;
class Scene;
class Intersection;

class Material
{
public:
	Material();
	virtual ~Material();

	virtual glm::vec3 calculateColor(const Rayon &r, Scene &sc, const Intersection & i) = 0;
};

