#! /usr/bin/python

import sys, os
from PIL import Image

img = Image.open(sys.argv[1])
img.save(''.join(os.path.splitext(sys.argv[1])[:-1])+".png")