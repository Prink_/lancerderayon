#include "const.hpp"
#include "camera.hpp"
#include "pixelmap.hpp"

#include "Objet3D.hpp"
#include "Sphere.h"
#include "Plane.h"
#include "Box.h"

#include "RMObject.h"

#include "AxisAlignedBoundingBox.h"

#include "Material.h"
#include "Phong.h"

#include "Attenuation.h"

#include "Light.h"
#include "PointLight.h"
#include "SphericalLight.h"

#include "rayon.hpp"
#include "RefractionRay.h"
#include "ShadowRay.h"



int main(void) {
	//omp_set_num_threads(7);
	omp_set_dynamic(true);
	
	// Une image Pixelmap
	Pixelmap pixmap(900, 670);
	//Pixelmap pixmap(1400, 1200);
	// Une image Bitmap
	Image bitmap;

	Scene scene;

	// Initialisation des objets==============================
	//Front - 
	Plane *front = new Plane(glm::vec3(0.0f, 0.0f, 50.0f), glm::vec3(0.0f,0.0f,-1.0f));
	front->mat = new Phong(glm::vec3(1.0f,1.0f,1.0f));
	//Bottom
	Plane *bottom = new Plane(glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f, 1.0f, 0.0f));
	bottom->mat = new Phong(glm::vec3(0.0f, 1.0f, 0.0f));
	bottom->Ka = 1.0f;
	bottom->Kfr = 0.0f;
	bottom->Kfl = 0.0f;

	Box * box = new Box(glm::vec3(-20.0f, 0.0f, 200.0f), glm::vec3(0.0f,0.0f,5.0f), glm::vec3(0.0f,60.0f,0.0f), glm::vec3(10.0f, 0.0f, 0.0f));
	box->mat = new Phong(glm::vec3(1.0f, 1.0f, 1.0f));

	Sphere *sph1 = new Sphere(glm::vec3(-5.0f, 5.0f, 20.0f), 2.0f);
	sph1->mat = new Phong(glm::vec3(1.0f), glm::vec3(0.2f, 0.8f, 0.2f));
	sph1->Ka = 0.2f;
	sph1->Kfr = 0.4f;
	sph1->Kfl = 0.0f;
	sph1->rIndex = 1.001f;
	sph1->color = glm::vec3(0.8f, 0.8f, 1.0f);

	Sphere *sph2 = new Sphere(glm::vec3(0.0f, 5.0f, 20.0f), 2.0f);
	sph2->mat = new Phong(glm::vec3(1.0f), glm::vec3(0.2f, 0.8f, 0.2f));
	sph2->Ka = 0.2f;
	sph2->Kfr = 0.4f;
	sph2->Kfl = 0.4f;
	sph2->rIndex = 1.33333f;
	sph2->color = glm::vec3(1.0f, 0.8f, 0.8f);

	Sphere *sph3 = new Sphere(glm::vec3(5.0f, 5.0f, 20.0f), 2.0f);
	sph3->mat = new Phong(glm::vec3(1.0f), glm::vec3(0.2f, 0.8f, 0.2f));
	sph3->Ka = 0.2f;
	sph3->Kfr = 0.4f;
	sph3->Kfl = 0.4f;
	sph3->rIndex = 1.6f;
	sph3->color = glm::vec3(0.8f, 1.0f, 0.8f);

	// Grid
	Sphere *sph;
	AxisAlignedBoundingBox *BB1, *BB2, *BB3;
	BB1 = new AxisAlignedBoundingBox();
	
	for (float i = 0.0f; i < 8.0f; i++) {
		BB2 = new AxisAlignedBoundingBox();
		for (float j = 0.0f; j < 10.0f; j++) {
			BB3 = new AxisAlignedBoundingBox();

			sph = new Sphere(glm::vec3((j*2.0f)-9.0f, (i * 2.0f) - 2.0f, 30.0f), 0.8f);
			sph->mat = new Phong(glm::vec3((i/10.0f),(j/10.0f),0.5f));
			BB3->ajouterObjet(sph);
			j++;

			sph = new Sphere(glm::vec3((j*2.0f) - 9.0f, (i * 2.0f) - 2.0f, 30.0f), 0.8f);
			sph->mat = new Phong(glm::vec3((i / 10.0f), (j / 10.0f), 0.5f));
			BB3->ajouterObjet(sph);

			BB2->ajouterObjet(BB3);
		}
		BB1->ajouterObjet(BB2);
	}

	Sphere * S1 = new Sphere(glm::vec3(0.0f, 5.0f, 25.0f),2.0f);
	Phong * phong = new Phong(glm::vec3(1.0f, 1.0f, 1.0f), glm::vec3(1.0f, 1.0f, 1.0f));
	phong->S = 4.0f;
	S1->color = glm::vec3(0.0f, 0.0f, 1.0f);
	S1->mat = phong;
	S1->Ka = 0.0f;
	S1->Kfl = 1.0f;
	//S1->Kfr = 1.0f;
	S1->attenuation = RM_noise;

	/*RM_Sphere * rms1 = new RM_Sphere();
	rms1->position = glm::vec3(0.0f, 5.0f, 20.0f);
	rms1->s = 1.0f;// glm::vec2(0.5f, 0.5f);
	rms1->mat = new Phong(glm::vec3(1.0f, 0.2f, 0.2f));*/
	RM_Capsule * capsule = new RM_Capsule();
	capsule->position = glm::vec3(0.0f, 5.0f, 20.0f);
	capsule->a = glm::vec3(-1.0f, 0.0f, 0.0f);
	capsule->b = glm::vec3(1.0f, 0.0f, 0.0f);
	capsule->r = 0.5f;
	capsule->rotation = glm::vec3(0.0f, 0.0f, 0.0f);
	capsule->mat = new Phong(glm::vec3(1.0f, 0.2f, 0.2f));
	capsule->maxRadius = 10.0f;

	RM_HexPrism * tp1 = new RM_HexPrism();
	tp1->position = glm::vec3(-4.0f, 5.0f, 20.0f);
	tp1->h = glm::vec2(1.5f, 1.0f);
	tp1->mat = new Phong(glm::vec3(0.2f, 1.0f, 0.2f));
	tp1->rotation = glm::vec3(0.0f, 0.5f, 0.0f);
	tp1->maxRadius = 2.0f;

	RM_Torus * torus = new RM_Torus();
	torus->position = glm::vec3(4.0f, 5.0f, 20.0f);
	torus->t = glm::vec2(1.5f,0.5f);
	torus->mat = new Phong(glm::vec3(0.2f, 0.2f, 1.0f));
	torus->rotation = glm::vec3(0.9f, 0.0f, 0.0f);
	torus->maxRadius = 1.8f;
	torus->Ka = 0.0f;
	torus->Kfr = 0.8f;
	torus->Kfl = 1.0f;

	// Ajouter les objets � la scene
	scene.ajouterObjet(front);
	scene.ajouterObjet(bottom);
	//scene.ajouterObjet(box);
	//scene.ajouterObjet(capsule);
	//scene.ajouterObjet(tp1);
	scene.ajouterObjet(torus);
	//scene.ajouterObjet(sph1);
	//scene.ajouterObjet(sph2);
	//scene.ajouterObjet(sph3);
	//scene.ajouterObjet(BB1);

	scene.ajouterObjet(S1);


	//Lighting
	scene.ambiantLight = glm::vec3(1.0f);
	SphericalLight * sphl = new SphericalLight(1.0f, glm::vec3(3.0f, 8.0f, 20.0f));

	//scene.addLight(sphl);
	//scene.ajouterObjet(sphl->sph);
	scene.addLight(new PointLight(glm::vec3(0.0f, 15.0f, 10.0f), glm::vec3(1.0f), 10));

	// la Camera
	Camera cam;
	cam.Centre(glm::vec3(0.0f, 5.0f, 12.0f));
	cam.Dir(glm::vec3(0.0f, 0.0f, 1.0f));
	cam.Haut(glm::vec3(0.0f, 1.0f, 0.0f));
	cam.Largeur((5.6f * 513.0f) / 384.0f);
	cam.Hauteur(5.6f);
	cam.SSnb = 1;
	cam.render = cam.SuperSampling;

	cam.Calculer_image(pixmap, scene, 6);

	pixmap.Transferer(bitmap);
	bitmap.Enregistrer("image.bmp");

	return 0;
}
