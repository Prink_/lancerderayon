#pragma once
#include "Light.h"
#include "const.hpp"
#include "Sphere.h"

class SphericalLight :
	public Light
{
public:
	SphericalLight(float radius, glm::vec3 pos, glm::vec3 color = glm::vec3(1.0f), float i = 10.0f);
	~SphericalLight();

	Sphere *sph;
	float radius;

	glm::vec3 incidentVector(glm::vec3 p);
};

