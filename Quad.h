#pragma once
#define SAME_SIGNS( a, b ) (((long) ((unsigned long) a ^ (unsigned long) b)) >= 0 )

#include "Objet3D.hpp"
class Quad :
	public Objet3D
{
private:
public:
	Quad();
	Quad(glm::vec3, glm::vec3, glm::vec3);
	Quad(glm::vec3, glm::vec3, glm::vec3, glm::vec3);
	~Quad();


	glm::vec3 normal;
	glm::vec3 H, W;
	bool calculIntersection(const Rayon &, std::vector<Intersection> & I);
	
};

