#pragma once
#include "Light.h"

class SurfaceLight : public Light
{
public:
	SurfaceLight();
	SurfaceLight(Objet3D *obj, glm::vec3 pos = glm::vec3(0.0f));
	SurfaceLight(Objet3D *obj, glm::vec3 pos = glm::vec3(0.0f), glm::vec3 color = glm::vec3(1.0f), float i = 10.0f);
	~SurfaceLight();

	Objet3D* surface;
};

