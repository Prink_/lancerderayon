#include "stdafx.h"
#include "PointLight.h"


PointLight::PointLight() : Light()
{
	this->position = glm::vec3(0.0);
	this->color = glm::vec3(1.0);
	this->intensity = 100;
}

PointLight::PointLight(glm::vec3 pos) : Light(pos)
{	
	this->color = glm::vec3(1.0);
	this->intensity = 100;
}

PointLight::PointLight(glm::vec3 pos, glm::vec3 col) : Light(pos, col)
{
	this->intensity = 100;
}

PointLight::PointLight(glm::vec3 pos, glm::vec3 col, float i) : Light(pos, col, i)
{	
}



PointLight::~PointLight()
{
}