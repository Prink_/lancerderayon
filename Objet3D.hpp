

#ifndef OBJECT_HPP_
#define OBJECT_HPP_


#include <vector>
#include "Intersection.hpp"
#include "Material.h"


class Rayon;
class Material;
class Intersection;
class Objet3D
{
 protected:


  
 public:

	 Objet3D() {}
	 Objet3D(glm::vec3 pos) { this->position = pos; }

     virtual bool calculIntersection(const Rayon & ,std::vector<Intersection> & I) = 0;
	 virtual glm::vec3 getMinPos() { return glm::vec3(INFINITY); }
	 virtual glm::vec3 getMaxPos() { return glm::vec3(-INFINITY); }
	 std::function<glm::vec3(glm::vec3)> attenuation = [this](glm::vec3 pos) { return 1.0f - this->color; };
	 
	 glm::vec3 getPositionObj(glm::vec3);
	 glm::vec3 getNormalWorld(glm::vec3);

	 glm::vec3 color;
	 Material * mat;
	 bool isLight = false;
	 float Kfr = 0.0f, Kfl = 0.0f, Ka = 1.0f, rIndex = 1.0f;
	 glm::vec3 position = glm::vec3(0.0f);
	 glm::vec3 rotation = glm::vec3(0.0f);
	 glm::vec3 scale = glm::vec3(1.0f);
};


#endif 
