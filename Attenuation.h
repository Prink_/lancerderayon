#pragma once
#include "const.hpp"

void SolidNoise(glm::vec3 point, glm::vec3 center, float radius) {
	// Transform to the point's local coordinate system
	glm::vec3 lsP = (point - center) / radius;
	glm::vec3 nsP = lsP;
	// Compute fractal function
	/*double fractalFunc = m_attrs.fractal->eval(nsP);
	fractalFunc = Math::gamma(fractalFunc, m_attrs.gamma.value());
	fractalFunc *= m_attrs.amplitude;
	// Compute final value
	double distanceFunc = 1.0 - lsP.length();
	sample.value = m_attrs.density.value() *
		std::max(0.0, distanceFunc + fractalFunc);*/
}

std::function<glm::vec3(glm::vec3)> RM_noise = [](glm::vec3 pos) {
	return 1.0f-glm::max(glm::vec3(glm::perlin(2.0f*pos)), 0.0f);
};