#ifndef CAMERA_HPP
#define CAMERA_HPP

#include <omp.h>

#include "const.hpp"
#include "pixelmap.hpp"
#include "rayon.hpp"
//#include "Rendering.hpp"

class Scene;

class Camera {
protected:

	glm::vec3 centre; // Centre de l'ecran de la camera
	float largeur; // Largeur de l'ecran
	float hauteur; // Hauteur de l'ecran
	float dist; // Distance focale de la camera
	glm::vec3 dir; // Direction de vue de la camera
	glm::vec3 haut; // Vecteur definissant l'orientation de la "tete" de la camera

public:

	Camera() {
		centre = glm::vec3(0, 0, -1);
		largeur = hauteur = 2;
		dist = 10;
		dir = glm::vec3(0, 0, 1);
		haut = glm::vec3(0, 1, 0);
		render = this->SimpleSampling;
	}

	glm::vec3 Centre() const {
		return centre;
	}
	float Largeur() const {
		return largeur;
	}
	float Hauteur() const {
		return hauteur;
	}
	float Dist() const {
		return dist;
	}
	glm::vec3 Dir() const {
		return dir;
	}
	glm::vec3 Haut() const {
		return haut;
	}

	void Centre(const glm::vec3 & c) {
		centre = c;
	}
	void Largeur(float l) {
		largeur = l;
	}
	void Hauteur(float h) {
		hauteur = h;
	}
	void Dist(float d) {
		dist = d;
	}
	void Dir(const glm::vec3 & d) {
		dir = d;
		dir = glm::normalize(dir);
	}

	void Haut(const glm::vec3 &h);

	void Calculer_image(Pixelmap &, Scene &sc, int) const;

	std::function<glm::vec3(int, int, float, float, glm::vec3, glm::vec3, glm::vec3, glm::vec3, Scene&, int)> render;


	int SSnb;
	//Rendering functions
	std::function<glm::vec3(int, int, float, float, glm::vec3, glm::vec3, glm::vec3, glm::vec3, Scene&, int)> SimpleSampling =
		[this](int x, int y, float dx, float dy, glm::vec3 hg, glm::vec3 haut, glm::vec3 droite, glm::vec3 foyer, Scene& sc, int recur) {
		glm::vec3 pt = hg + (droite * (dx * x)) - (haut * (dy * y));
		glm::vec3 vect = glm::normalize(pt - foyer);

		Rayon ray;
		ray.Orig(pt);
		ray.Vect(vect);
		
		return ray.Lancer(sc, recur);
	};

	std::function<glm::vec3(int, int, float, float, glm::vec3, glm::vec3, glm::vec3, glm::vec3, Scene&, int)> SuperSampling =
		[this](int x, int y, float dx, float dy, glm::vec3 hg, glm::vec3 haut, glm::vec3 droite, glm::vec3 foyer, Scene& sc, int recur) {
		glm::vec3 pt, vect;
		glm::vec3 res;

		Rayon ray;
		for (int i = 0; i < this->SSnb; i++) {
			for (int j = 0; j < this->SSnb; j++) {
				pt = hg + (droite * ((dx * x)+(dx*i/SSnb))) - (haut * (dy * y)+(dy*j/SSnb));
				vect = glm::normalize(pt - foyer);
				ray.Orig(pt);
				ray.Vect(vect);
				res += ray.Lancer(sc, recur) / (float)(SSnb*SSnb);
			}
		}
		
		return res;
	};
	std::function<glm::vec3(int, int, float, float, glm::vec3, glm::vec3, glm::vec3, glm::vec3, Scene&, int)> AdaptiveSampling =
		[this](int x, int y, float dx, float dy, glm::vec3 hg, glm::vec3 haut, glm::vec3 droite, glm::vec3 foyer, Scene& sc, int recur) {
		glm::vec3 pt, vect;
		glm::vec3 res;

		Rayon ray;
		for (int i = 0; i < this->SSnb; i++) {
			for (int j = 0; j < this->SSnb; j++) {
				pt = hg + (droite * ((dx * x) + (dx*i / 3))) - (haut * (dy * y) + (dy*j / 3));
				vect = glm::normalize(pt - foyer);
				ray.Orig(pt);
				ray.Vect(vect);
				res += ray.Lancer(sc, recur) / (float)(SSnb*SSnb);
			}
		}

		return res;
	};
};

#endif
