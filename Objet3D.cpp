#include "rayon.hpp"

//Transforme la position en entr�e dans le rep�re de l'objet
glm::vec3 Objet3D::getPositionObj(glm::vec3 wPos)
{
	//Retour � l'origine
	glm::vec3 pObj = wPos - this->position;
	//Rotation
	pObj = pObj * glm::mat3(1.0f,0.0f,0.0f,0.0f,cos(this->rotation.x),-sin(this->rotation.x),0.0f,sin(this->rotation.x),cos(this->rotation.x)) *
		glm::mat3(cos(this->rotation.y),0.0f,sin(this->rotation.y),0.0f,1.0f,0.0f,-sin(this->rotation.y),0.0f,cos(this->rotation.y)) *
		glm::mat3(cos(this->rotation.z),-sin(this->rotation.z),0.0f,sin(this->rotation.z),cos(this->rotation.z),0.0f,0.0f,0.0f,1.0f);
	//Homoth�tie
	pObj = pObj * glm::mat3(this->scale.x, 0.0f, 0.0f, 0.0f, this->scale.y, 0.0f, 0.0f, 0.0f, this->scale.z);
	return pObj;
}

glm::vec3 Objet3D::getNormalWorld(glm::vec3 objN)
{
	glm::vec3 N;
	N = objN * glm::inverse(glm::mat3(1.0f, 0.0f, 0.0f, 0.0f, cos(this->rotation.x), -sin(this->rotation.x), 0.0f, sin(this->rotation.x), cos(this->rotation.x))) *
		glm::inverse(glm::mat3(cos(this->rotation.y), 0.0f, sin(this->rotation.y), 0.0f, 1.0f, 0.0f, -sin(this->rotation.y), 0.0f, cos(this->rotation.y))) *
			glm::inverse(glm::mat3(cos(this->rotation.z), -sin(this->rotation.z), 0.0f, sin(this->rotation.z), cos(this->rotation.z), 0.0f, 0.0f, 0.0f, 1.0f));
	return N;
}
