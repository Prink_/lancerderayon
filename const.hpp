#ifndef CLASSES_HPP
#define CLASSES_HPP

#include <cmath>
#include <vector>
#include <stdio.h>
#include <iostream>
#include <functional>
#include <unordered_map>

#include <glm\glm.hpp>
#include <glm\gtc\noise.hpp>
#include <glm\gtc\random.hpp>
typedef unsigned char byte;
typedef unsigned short word;
typedef unsigned int dword;

// CONSTANTES

#define EPSILON 0.0001f
#define PI acosf(-1)
//#define INFINITY std::numeric_limits<float>::infinity();
#endif
