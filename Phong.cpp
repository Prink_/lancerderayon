#include "stdafx.h"
#include "Phong.h"


Phong::Phong(glm::vec3 color) {
	this->Ca = color;
	this->Cd = color;
	this->Cs = color;
}

Phong::Phong(glm::vec3 color, glm::vec3 coef) {
	this->Ca = color;
	this->Cd = color;
	this->Cs = color;

	this->Ka = coef.x;
	this->Kd = coef.y;
	this->Ks = coef.z;
}

Phong::Phong(glm::vec3 ambiantColor, glm::vec3 difuseColor, glm::vec3 specularColor)
{
	this->Ca = ambiantColor;
	this->Cd = difuseColor;
	this->Cs = specularColor;
}

Phong::Phong(glm::vec3 ambiantColor, glm::vec3 difuseColor, glm::vec3 specularColor, glm::vec3 coef) {
	this->Ca = ambiantColor;
	this->Cd = difuseColor;
	this->Cs = specularColor;

	this->Ka = coef.x;
	this->Kd = coef.y;
	this->Ks = coef.z;
}

Phong::Phong(glm::vec3 ambiantColor, glm::vec3 difuseColor, glm::vec3 specularColor, float ambiantCoef, float difuseCoef, float specularCoef)
{
	this->Ca = ambiantColor;
	this->Cd = difuseColor;
	this->Cs = specularColor;

	this->Ka = ambiantCoef;
	this->Kd = difuseCoef;
	this->Ks = specularCoef;
}


Phong::~Phong()
{
}

glm::vec3 Phong::calculateColor(const Rayon &r, Scene &sc, const Intersection & i)
{
	glm::vec3 color = glm::vec3(0.0f);
	glm::vec3 N = i.normal;

	float Kid, Kis;

	ShadowRay R = ShadowRay();
	std::vector< std::vector<Intersection> > interectedObjects;
	Intersection I;
	for (auto l : sc.lightList) {
		Kid = this->Kd;
		Kis = this->Ks;

		glm::vec3 L = l->incidentVector(i.position);
		
		R.Orig(i.position + -L * 0.01f);
		R.Vect(-L);
		R.lightDist = glm::distance(i.position, l->position);
		float kMin = R.Lancer(sc,1).x;
		
		Kid = this->Kd * kMin;
		if(kMin<1.0f)
			Kis = 0.0f;

		glm::vec3 Ia = this->Ca * sc.ambiantLight;
		glm::vec3 Id = this->Cd * std::max(glm::dot(-L,N), 0.0f) * l->color;
		float alpha = glm::dot(r.Vect(), glm::reflect(-L,N));
		glm::vec3 Is = this->Cs * powf(std::max(alpha, 0.0f),this->S) * l->color;

		color += Ka * Ia + Kid * Id + Kis * Is;
	}
	return color;
}
