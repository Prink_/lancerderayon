#include "stdafx.h"
#include "AxisAlignedBoundingBox.h"


AxisAlignedBoundingBox::AxisAlignedBoundingBox()
{
	this->minPos = glm::vec3(INFINITY);
	this->maxPos = glm::vec3(-INFINITY);
}


AxisAlignedBoundingBox::~AxisAlignedBoundingBox()
{
}


//Algorithm from http://www.siggraph.org/education/materials/HyperGraph/raytrace/rtinter3.htm
bool AxisAlignedBoundingBox::calculIntersection(const Rayon &ray, std::vector<Intersection>& I)
{
	float t1, t2, tnear = -INFINITY, tfar = INFINITY;

	for (int i = 0; i < 3; i++) { //Pour chaque axe
		if (ray.Vect()[i] == 0.0f && !(minPos[i] <= ray.Orig()[i] && ray.Orig()[i] <= maxPos[i])) {
			return false;
		}
		else {
			t1 = (this->minPos[i] - ray.Orig()[i]) / ray.Vect()[i];
			t2 = (this->maxPos[i] - ray.Orig()[i]) / ray.Vect()[i];

			if (t1 > t2) std::swap(t1, t2);
			if (t1 > tnear) tnear = t1;
			if (t2 < tfar) tfar = t2;
			if (tnear > tfar) return false;
			if (tfar < 0) return false;
		}
	}

	for (auto o : this->subItems) {
		o->calculIntersection(ray, I);
	}
	return true;
}

void AxisAlignedBoundingBox::ajouterObjet(Objet3D *obj)
{
	for (int i = 0; i < 3; i++) {
		if (obj->getMinPos()[i] < this->minPos[i]) this->minPos[i] = obj->getMinPos()[i] - EPSILON;
		if (obj->getMaxPos()[i] > this->maxPos[i]) this->maxPos[i] = obj->getMaxPos()[i] + EPSILON;
	}
	
	this->subItems.push_back(obj);
}
