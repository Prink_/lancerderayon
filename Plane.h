#pragma once
#include "Objet3D.hpp"
class Plane :
	public Objet3D
{
public:
	Plane();
	Plane(glm::vec3 position);
	Plane(glm::vec3 position, glm::vec3 normale);
	~Plane();

	glm::vec3 normal;

	bool calculIntersection(const Rayon &, std::vector<Intersection> & I);
};

