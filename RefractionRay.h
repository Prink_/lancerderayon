#pragma once
#include "rayon.hpp"
class RefractionRay : public Rayon
{
public:
	RefractionRay() {}
	~RefractionRay() {}

	glm::vec3 Lancer(Scene &sc, int recur) const;

	Intersection I1;
};

