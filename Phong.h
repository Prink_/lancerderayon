#pragma once
#include "Material.h"
#include "ShadowRay.h"

/**
	Ka * Ambiant + Kd * Diffus + Ks * Spec
	Ambiant
	Difus

*/
class Phong : public Material
{
public:
	Phong() {};
	Phong(glm::vec3);
	Phong(glm::vec3, glm::vec3);
	Phong(glm::vec3, glm::vec3, glm::vec3);
	Phong(glm::vec3, glm::vec3, glm::vec3, glm::vec3);
	Phong(glm::vec3, glm::vec3, glm::vec3, float, float, float);
	~Phong();

	glm::vec3 Ca = glm::vec3(1.0f), Cd = glm::vec3(1.0f), Cs = glm::vec3(1.0f);
	float Ka = 0.1f, Kd = 0.9f, Ks = 0.4f, S = 4.0f;

	glm::vec3 calculateColor(const Rayon &, Scene &, const Intersection &);
};

