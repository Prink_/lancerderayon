#pragma once
#include "Light.h"
class PointLight :
	public Light
{
public:
	PointLight();
	PointLight(glm::vec3 pos);
	PointLight(glm::vec3 pos, glm::vec3 col);
	PointLight(glm::vec3 pos, glm::vec3 col, float i);
	~PointLight();
};

