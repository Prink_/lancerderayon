#include "stdafx.h"
#include "Sphere.h"


Sphere::Sphere()
{
}

Sphere::Sphere(glm::vec3 pos, float radius)
{
	this->position = pos;
	this->radius = radius;
}


Sphere::~Sphere()
{
}

bool Sphere::calculIntersection(const Rayon & ray, std::vector<Intersection> & I)
{
	glm::vec3 I1, I2;
	glm::vec3 diffOC = ray.Orig() - this->position;
	float t1, t2;
	float a = glm::dot(ray.Vect(), ray.Vect());
	float b = 2 * glm::dot(ray.Vect(), diffOC);
	float c = glm::dot(diffOC, diffOC) - powf(radius,2.0f);

	float delta = powf(b,2.0f) - 4 * a*c;
	

	t1 = t2 = -INFINITY;
	if (delta >= 0) {
		t1 = (-b + sqrt(delta)) / 2.0f * a;
	}
	if (delta > 0) {
		t2 = (-b - sqrt(delta)) / 2.0f * a;
		
	}
	if (t1 > 0.0f || t2 > 0.0f) {
		if (t1 < t2)
			std::swap(t1, t2);

		if (t1 > 0.0f) {
			I1 = ray.IPosition(t1);
			I.push_back(Intersection(t1, I1, glm::normalize(I1 - this->position), this));
		}
		if (t2 > 0.0f) {
			I2 = ray.IPosition(t2);
			I.push_back(Intersection(t2, I2, glm::normalize(I2 - this->position), this));
		}
		return true;
	}

	return false;
}
