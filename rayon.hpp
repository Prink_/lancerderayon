#ifndef RAYON_HPP_
#define RAYON_HPP_

#include "const.hpp"
#include "Scene.hpp"

class Scene;
class Light;

class Rayon {
protected:

	glm::vec3 orig; // Origine du rayon
	glm::vec3 vect; // Vecteur directeur (oriente) du rayon

	std::vector<Intersection> intersectScene(Scene&) const;
	Intersection closestIntersection(std::vector<Intersection>) const;
	glm::vec3 calculateIntersectionColor(Intersection, Scene&, int) const;

public:

	Rayon() { this->rIndex = 1.0; }
	float rIndex;

	// Lecture
	glm::vec3 Orig() const {
		return orig;
	}
	glm::vec3 Vect() const {
		return vect;
	}

	// Ecriture
	void Orig(const glm::vec3 & o) {
		orig = o;
	}
	void Vect(const glm::vec3 & v) {
		vect = glm::normalize(v);
	}

	glm::vec3 IPosition(float) const;

	glm::vec3 Lancer(Scene &sc,int recur) const;
};

#endif /* RAYON_HPP_ */
