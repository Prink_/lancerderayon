#pragma once
#include <glm/glm.hpp>
#include "Objet3D.hpp"
class Light
{
public:

	/*Light() {}
	Light(glm::vec3 pos = glm::vec3(0.0f)) { this->position = pos; };
	Light(glm::vec3 pos = glm::vec3(0.0f), glm::vec3 col = glm::vec3(1.0f)) {
		this->position = pos;
		this->color = col;
	};*/
	Light(glm::vec3 pos = glm::vec3(0.0f), glm::vec3 col = glm::vec3(1.0f), float i = 10.0f) {
		this->position = pos;
		this->color = col;
		this->intensity = i;
	};
	~Light() {}

	float intensity;

	glm::vec3 position;
	glm::vec3 color;

	virtual glm::vec3 incidentVector(glm::vec3 P) { return glm::normalize(P-this->position); }; //

};

