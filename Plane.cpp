#include "stdafx.h"
#include "Plane.h"


Plane::Plane() : Objet3D()
{
	this->position = glm::vec3(0.0, 0.0, 0.0);
	this->normal = glm::normalize(glm::vec3(0.0, 1.0, 0.0));
	this->color = glm::vec3(0.2, 0.4, 0.8);
}
Plane::Plane(glm::vec3 position) : Objet3D(position)
{
	this->position = position;
	this->normal = glm::normalize(glm::vec3(0.0,1.0,0.0));
	this->color = glm::vec3(0.2, 0.4, 0.8);
}

Plane::Plane(glm::vec3 position, glm::vec3 normale) : Objet3D(position)
{
	this->position = position;
	this->normal = glm::normalize(normale);
	this->color = glm::vec3(0.2,0.4,0.8);
}


Plane::~Plane()
{
}

bool Plane::calculIntersection(const Rayon &ray, std::vector<Intersection>& I)
{
	//Parallelism test. Coincidence is not rendered thus return false as well.
	float theta = glm::dot(this->normal, ray.Vect());
	if (theta != 0) {
		Intersection i = Intersection();
		i.normal = this->normal;
		i.Objet = this;
		i.dist = glm::dot(this->normal, this->position - ray.Orig()) / theta;
		i.position = ray.Orig() + i.dist * ray.Vect();
		
		if (i.dist > 0) {
			//std::cout << theta << std::endl;
			I.push_back(i);
			return true;
		}
	}

	return false;
}