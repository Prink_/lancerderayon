#pragma once
#include "const.hpp"
#include "Objet3D.hpp"


class AxisAlignedBoundingBox : public Objet3D {
public:
	AxisAlignedBoundingBox();
	~AxisAlignedBoundingBox();

	bool calculIntersection(const Rayon &, std::vector<Intersection>&);
	void ajouterObjet(Objet3D *);

	glm::vec3 minPos, maxPos;
	std::vector<Objet3D*> subItems;
};