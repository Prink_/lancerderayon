#include "stdafx.h"
#include "Box.h"


Box::Box()
{
	std::cerr << "B.O.X.cpp: No default values..." << std::endl;
}

Box::Box(glm::vec3 O, glm::vec3 x, glm::vec3 y, glm::vec3 z)
{
	this->position = O;

	this->faces.push_back(Quad(O + y, O + y + z, O + x + y));	//Top
	this->faces.push_back(Quad(O, O + x, O + z));				//Bottom

	this->faces.push_back(Quad(O, O + z, O + y));				//Left
	this->faces.push_back(Quad(O + x, O + x + y, O + x + z));	//Right

	this->faces.push_back(Quad(O, O + y, O + x));				//Front
	this->faces.push_back(Quad(O + z, O + x + z, O + y + z));	//Back
}


Box::~Box()
{
}

bool Box::calculIntersection(const Rayon &ray, std::vector<Intersection>& iList)
{
	std::vector<Intersection> I;
	for (auto face : faces) {
		face.calculIntersection(ray, I);
	}
	if (I.empty())
		return false;
	for (auto i : I) {
		i.Objet = this;
		iList.push_back(i);
	}
	return true;
}