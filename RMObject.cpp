#include "stdafx.h"
#include "RMObject.h"

/*===================================
	Distance function by Inigo Quilez
	http://iquilezles.org/www/articles/distfunctions/distfunctions.htm
  ===================================*/

using namespace glm;

RM_Object::RM_Object()
{
}

RM_Object::~RM_Object()
{
}

bool RM_Object::calculIntersection(const Rayon & ray, std::vector<Intersection>& I)
{
	float outerLimit = 2.0f * glm::distance(ray.Orig(), this->position) + maxRadius;
	float d, step = 0.01f;
	vec3 p, N = vec3(0.0f, 0.0f, 0.0f);
	float s = sign(step);
	for (float i = 0.9f * distance(getPositionObj(ray.IPosition(0.0f))); i < outerLimit; i += step) {
		p = getPositionObj(ray.IPosition(i));
		d = distance(p);
		//if (glm::sign(d) != sign) {
		if (abs(d)<=0.001f) {
			//Calculate normal
			N.x = distance(p + vec3(EPSILON, 0.0f, 0.0f)) -
				distance(p + vec3(-EPSILON, 0.0f, 0.0f));
			N.y = distance(p + vec3(0.0f, EPSILON, 0.0f)) -
				distance(p + vec3(0.0f, -EPSILON, 0.0f));
			N.z = distance(p + vec3(0.0f, 0.0f, EPSILON)) -
				distance(p + vec3(0.0f, 0.0f, -EPSILON));
			N = normalize(N);
			
			I.push_back(Intersection(i, ray.IPosition(i), this->getNormalWorld(N), this));
			//printf("wah!");
			return true;
		}
		if (sign(step) != s) {
			s = -s;
			step /= -2.0f;
		}
	}
	
	return false;
}

float dot2(glm::vec3 v) {
	return glm::dot(v, v);
}

float RM_Sphere::distance(glm::vec3 p)
{
	return length(p) - s;
}

float RM_UBox::distance(glm::vec3 p)
{
	return length(max(abs(p) - b, 0.0f));
}

float RM_RoundBox::distance(glm::vec3 p)
{
	return length(max(abs(p) - b, 0.0f)) - r;
}

float RM_Box::distance(glm::vec3 p)
{
	vec3 d = abs(p) - b;
	return min(max(d.x, max(d.y, d.z)), 0.0f) + length(max(d, 0.0f));
}

float RM_Torus::distance(glm::vec3 p)
{
	vec2 q = vec2(length(vec2(p.x,p.z)) - t.x, p.y);
	return length(q) - t.y;
}

float RM_Cylinder::distance(glm::vec3 p)
{
	return length(vec2(p.x,p.z) - vec2(c.x,p.y)) - c.z;
}

float RM_Cone::distance(glm::vec3 p)
{
	// c must be normalized
	//c = normalize(c);
	float q = length(vec2(p.x,p.y));
	return dot(c, vec2(q, p.z));
}

float RM_Plane::distance(glm::vec3 p)
{
	// n must be normalized
	n = normalize(n);
	return dot(p, vec3(n.x, n.y, n.z)) + n.w;
}

float RM_HexPrism::distance(glm::vec3 p)
{
	vec3 q = abs(p);
	return max(q.z - h.y, max((q.x*0.866025f + q.y*0.5f), q.y) - h.x);
}

float RM_TriPrism::distance(glm::vec3 p)
{
	vec3 q = abs(p);
	return max(q.z - h.y, max(q.x*0.866025f + p.y*0.5f, -p.y) - h.x*0.5f);
}

float RM_Capsule::distance(glm::vec3 p)
{
	vec3 pa = p - a, ba = b - a;
	float h = clamp(dot(pa, ba) / dot(ba, ba), 0.0f, 1.0f);
	return length(pa - ba*h) - r;
}

float RM_CappedCylinder::distance(glm::vec3 p)
{
	vec2 d = abs(vec2(length(vec2(p.x,p.z)), p.y)) - h;
	return min(max(d.x, d.y), 0.0f) + length(max(d, 0.0f));
}

float RM_CappedCone::distance(glm::vec3 p)
{
	vec2 q = vec2(length(vec2(p.x, p.z)), p.y);
	vec2 v = vec2(c.z*c.y / c.x, -c.z);
	vec2 w = v - q;
	vec2 vv = vec2(dot(v, v), v.x*v.x);
	vec2 qv = vec2(dot(v, w), v.x*w.x);
	vec2 d = max(qv, 0.0f)*qv / vv;
	return sqrt(dot(w, w) - max(d.x, d.y))* sign(max(q.y*v.x - q.x*v.y, w.y));
}

float RM_Ellipsoid::distance(glm::vec3 p)
{
	return (length(p / r) - 1.0f) * min(min(r.x, r.y), r.z);
}

float RM_UTriangle::distance(glm::vec3 p)
{
	vec3 ba = b - a; vec3 pa = p - a;
	vec3 cb = c - b; vec3 pb = p - b;
	vec3 ac = a - c; vec3 pc = p - c;
	vec3 nor = cross(ba, ac);

	return sqrt(
		(sign(dot(cross(ba, nor), pa)) +
			sign(dot(cross(cb, nor), pb)) +
			sign(dot(cross(ac, nor), pc))<2.0)
		?
		min(min(
			dot2(ba*clamp(dot(ba, pa) / dot2(ba), 0.0f, 1.0f) - pa),
			dot2(cb*clamp(dot(cb, pb) / dot2(cb), 0.0f, 1.0f) - pb)),
			dot2(ac*clamp(dot(ac, pc) / dot2(ac), 0.0f, 1.0f) - pc))
		:
		dot(nor, pa)*dot(nor, pa) / dot2(nor));
}

float RM_UQuad::distance(glm::vec3 p)
{
	vec3 ba = b - a; vec3 pa = p - a;
	vec3 cb = c - b; vec3 pb = p - b;
	vec3 dc = d - c; vec3 pc = p - c;
	vec3 ad = a - d; vec3 pd = p - d;
	vec3 nor = cross(ba, ad);

	return sqrt(
		(sign(dot(cross(ba, nor), pa)) +
			sign(dot(cross(cb, nor), pb)) +
			sign(dot(cross(dc, nor), pc)) +
			sign(dot(cross(ad, nor), pd))<3.0f)
		?
		min(min(min(
			dot2(ba*clamp(dot(ba, pa) / dot2(ba), 0.0f, 1.0f) - pa),
			dot2(cb*clamp(dot(cb, pb) / dot2(cb), 0.0f, 1.0f) - pb)),
			dot2(dc*clamp(dot(dc, pc) / dot2(dc), 0.0f, 1.0f) - pc)),
			dot2(ad*clamp(dot(ad, pd) / dot2(ad), 0.0f, 1.0f) - pd))
		:
		dot(nor, pa)*dot(nor, pa) / dot2(nor));
}
