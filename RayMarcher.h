#pragma once
#include "rayon.hpp"

class Scene;
class Light;

std::unordered_map<std::string, std::function<bool(glm::vec3())>> object;

class RayMarcher : public Rayon
{
public:
	RayMarcher();
	~RayMarcher();

	Intersection I1, I2;
	float step;
	bool lancer(Scene &sc, int recur);

	template <typename Inter>
	bool march(Inter I);
};

