#include "stdafx.h"
#include "ShadowRay.h"
#include "RefractionRay.h"

ShadowRay::ShadowRay()
{
	lightDist = 0;
}


ShadowRay::~ShadowRay()
{
}

glm::vec3 ShadowRay::Lancer(Scene &sc, int recur) const {
	Intersection I;

	std::vector<Intersection> interectedObjects = std::vector<Intersection>(intersectScene(sc));

	I.Objet = NULL;

	float kMin = 1.0;
	for (auto i : interectedObjects) {
		if (i.Objet->Kfr < kMin && i.dist < lightDist && !i.Objet->isLight) {
			kMin = i.Objet->Kfr;
		}
	}

	return glm::vec3(kMin);
}