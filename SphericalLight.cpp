#include "stdafx.h"
#include "SphericalLight.h"


SphericalLight::SphericalLight(float radius, glm::vec3 pos, glm::vec3 color, float i)
	: Light(pos, color, i) {
	this->sph = new Sphere(pos, radius);
	this->radius = radius;

	this->sph->isLight = true;
	this->sph->color = color;
}


SphericalLight::~SphericalLight()
{
}

glm::vec3 SphericalLight::incidentVector(glm::vec3 P)
{
	return glm::normalize(P - this->position + glm::ballRand(this->radius));
}
