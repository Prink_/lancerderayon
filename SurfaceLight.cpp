#include "stdafx.h"
#include "SurfaceLight.h"


SurfaceLight::SurfaceLight() : Light()
{
}

SurfaceLight::SurfaceLight(Objet3D *obj, glm::vec3 pos) : Light(pos)
{
	this->surface = obj;
}

SurfaceLight::SurfaceLight(Objet3D * obj, glm::vec3 pos, glm::vec3 color, float i) : Light(pos, color, i)
{
	this->surface = obj;
}


SurfaceLight::~SurfaceLight()
{
}
