#include "RefractionRay.h"

glm::vec3 RefractionRay::Lancer(Scene & sc, int recur) const
{
	glm::vec3 res; // Couleur resultante	
	Intersection I2;

	std::vector<Intersection> interectedObjects = std::vector<Intersection>(intersectScene(sc));

	// Traiter l'intersection la plus proche de l'origine
	I2.Objet = NULL;
	I2 = closestIntersection(interectedObjects);

	res = sc.bgColor;
	if (I1.Objet == I2.Objet) {
		//Lancer un rayon depuis la seconde intersection
		Rayon R2 = Rayon();
		R2.Vect(glm::refract(glm::normalize(I2.position - I1.position), -I2.normal, I1.Objet->rIndex));
		R2.Orig(I2.position + R2.Vect() * EPSILON);
		R2.rIndex = I2.Objet->rIndex;
		res = R2.Lancer(sc, recur-1);
	} else {
		//L'intersection est soit contenue dans l'objet de I1, soit ce dernier n'a pas de volume.
		
		res = calculateIntersectionColor(I2, sc, recur);
	}

	float distance = glm::distance(I1.position, I2.position);
	float step = 0.01f; ///TODO: calculate step
	//float Ai = 0.1f * step;
	auto Ci = res;
	//Volumetric raymarching
	if (I2.Objet != NULL) {
		for (float i = 0.0f; i < distance; i += step) {
			Ci = I1.Objet->color * glm::dot(i, step) * step + glm::exp(-I1.Objet->attenuation(I2.position + -this->Vect() * i) * step) * Ci;
		}
		res = Ci;
	}
	return res;
}