#pragma once
#include "Objet3D.hpp"

class RM_Object :
	public Objet3D
{
public:
	RM_Object();
	~RM_Object();

	bool calculIntersection(const Rayon &, std::vector<Intersection> & I);
	virtual float distance(glm::vec3 p) = 0;
	float maxRadius = 1.0f;
	float minStep = 0.001f;
};

float dot2(glm::vec3);

class RM_Sphere : public RM_Object 
{
public:
	float distance(glm::vec3 p);
	float s = 1.0f;
};

class RM_UBox : public RM_Object
{
public:
	float distance(glm::vec3 p);
	glm::vec3 b = glm::vec3(1.0f);
};

class RM_RoundBox : public RM_Object
{
public:
	float distance(glm::vec3 p);
	glm::vec3 b = glm::vec3(1.0f);
	float r;
};

class RM_Box : public RM_Object
{
public:
	float distance(glm::vec3 p);
	glm::vec3 b = glm::vec3(1.0f);
};

class RM_Torus : public RM_Object
{
public:
	float distance(glm::vec3 p);
	glm::vec2 t = glm::vec2(1.0f);
};

class RM_Cylinder : public RM_Object
{
public:
	float distance(glm::vec3 p);
	glm::vec3 c = glm::vec3(1.0f);
};

class RM_Cone : public RM_Object
{
public:
	float distance(glm::vec3 p);
	glm::vec2 c = glm::vec2(1.0f);
};

class RM_Plane : public RM_Object
{
public:
	float distance(glm::vec3 p);
	glm::vec4 n = glm::vec4(0.0f,1.0f,0.0f,0.0f);
};

class RM_HexPrism : public RM_Object
{
public:
	float distance(glm::vec3 p);
	glm::vec2 h = glm::vec2(1.0f);
};

class RM_TriPrism : public RM_Object
{
public:
	float distance(glm::vec3 p);
	glm::vec2 h = glm::vec2(1.0f);
};

class RM_Capsule : public RM_Object
{
public:
	float distance(glm::vec3 p);
	glm::vec3 a = glm::vec3(1.0f);
	glm::vec3 b = glm::vec3(1.0f);
	float r;
};

class RM_CappedCylinder : public RM_Object
{
public:
	float distance(glm::vec3 p);
	glm::vec2 h = glm::vec2(1.0f);
};

class RM_CappedCone : public RM_Object
{
public:
	float distance(glm::vec3 p);
	glm::vec3 c = glm::vec3(1.0f);
};

class RM_Ellipsoid : public RM_Object
{
public:
	float distance(glm::vec3 p);
	glm::vec3 r = glm::vec3(1.0f);
};

class RM_UTriangle : public RM_Object
{
public:
	float distance(glm::vec3 p);
	glm::vec3 a = glm::vec3(1.0f);
	glm::vec3 b = glm::vec3(1.0f);
	glm::vec3 c = glm::vec3(1.0f);
};

class RM_UQuad : public RM_Object
{
public:
	float distance(glm::vec3 p);
	glm::vec3 a = glm::vec3(1.0f);
	glm::vec3 b = glm::vec3(1.0f);
	glm::vec3 c = glm::vec3(1.0f);
	glm::vec3 d = glm::vec3(1.0f);
};