# LancerDeRayon

Projet dans le cadre du module d’Introduction à la Synthèse d’Images Réalistes.

Le but est d’implémenter un moteur de rendu utilisant le [Ray Tracing]( https://fr.wikipedia.org/wiki/Ray_tracing). Toute la partie liée au fonctionnement interne du rayon a été développé manuellement. Le processus est réalisé sur le CPU utilisant tous les cœurs disponibles (via OpenMP).

**ATTENTION** : Si vous souhaitez lancer le programme, le rendu est *TRES* long, potentiellement plusieurs heures.

Quelques exemples de rendu :

![Rendu Final](https://gitlab.com/Prink_/lancerderayon/raw/master/Autre/HDaa.jpg)

Rendu final avec un anti-aliasing appliqué par-dessus.

![](https://gitlab.com/Prink_/lancerderayon/raw/master/Autre/001.jpg)

Rendu d'une simple sphère avec un ombrage de Phong.

![](https://gitlab.com/Prink_/lancerderayon/raw/master/Autre/002.jpg)

![](https://gitlab.com/Prink_/lancerderayon/raw/master/Autre/003.jpg)

L'ombrage est calculé grâce à des rayons secondaire.

![](https://gitlab.com/Prink_/lancerderayon/raw/master/Autre/004.jpg)

La sphère du centre transmet la lumière par réfraction comme un boulle de verre, ce quiexplique l'inversion de l'image au travers d'elle.

La seconde sphère en bas à gauche reflète la scène comme un miroir.

![](https://gitlab.com/Prink_/lancerderayon/raw/master/Autre/005.jpg)

Chaque sphère présente un coefficient de réfraction diffèrent. 

Celui-ci augmente de gauche à droite.

![](https://gitlab.com/Prink_/lancerderayon/raw/master/Autre/006.jpg)

Ici le but est de s'assurer que les différents effets marchent ensemble dans la même scène. 

Ici on rajouter une boite autour de la scène avec un miroir en face.

![](https://gitlab.com/Prink_/lancerderayon/raw/master/Autre/007.jpg)

![](https://gitlab.com/Prink_/lancerderayon/raw/master/Autre/008.jpg)

![](https://gitlab.com/Prink_/lancerderayon/raw/master/Autre/009.jpg)

Les objets sont rendus via une méthode de [Ray Marching]( https://en.wikipedia.org/wiki/Volume_ray_casting). Cela permet d'afficher des objets plus complexes sans passer par de la modélisation.

Chaque objet est défini via une fonction distance qui détermine l'espace minimum entre la position actuelle du rayon et l'objet.
[Source des fonctions de distance](http://iquilezles.org/www/articles/distfunctions/distfunctions.htm)

![](https://gitlab.com/Prink_/lancerderayon/raw/master/Autre/010.jpg)