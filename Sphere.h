#pragma once
#include "const.hpp"
#include "Objet3D.hpp"

class Sphere :
	public Objet3D
{
public:
	Sphere();
	Sphere(glm::vec3, float);
	~Sphere();

	bool calculIntersection(const Rayon &, std::vector<Intersection> & I);

	float radius;
};

