#pragma once
#include "Objet3D.hpp"
#include "Quad.h"

class Box :
	public Objet3D
{
public:
	Box();
	Box(glm::vec3, glm::vec3, glm::vec3, glm::vec3);
	~Box();
	//u, d, l, r, f, b
	std::vector <Quad> faces;
	bool calculIntersection(const Rayon &, std::vector<Intersection> & I);
};

